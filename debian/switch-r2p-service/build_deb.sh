#!/bin/bash

# This is a hacky little script to generate a deb.
#
# NOTE: This only works for building an arm64 .deb as is. Change
#       the Architecture below if desired.

architecture="arm64"
package="$(pwd)/build/deb/switch-r2p-service"
package_version="1.0"

mkdir -p "${package}/usr/lib/firmware" "${package}/etc/systemd/system/"

install -Dm 755 reboot_payload.bin "${package}/usr/lib/firmware"
install -Dm 755 r2p.service "${package}/etc/systemd/system"

cd "${package}" || exit

mkdir -p DEBIAN
cd DEBIAN || exit

cat <<EOF > control
Package: switch-r2p-service
Architecture: $architecture
Maintainer: Azkali Manad
Homepage: https://gitlab.com/switchroot
Priority: optional
Version: $package_version
Description: Reboot to Payload service for the Nintendo Switch
EOF

cat <<EOF > conffiles
EOF

cd ../..

dpkg-deb --build switch-r2p-service || exit
mv switch-r2p-service.deb switch-r2p-service_${package_version}.deb
