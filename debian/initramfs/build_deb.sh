#!/bin/bash

# This is a hacky little script to generate a deb.
#
# NOTE: This only works for building an arm64 .deb as is. Change
#       the Architecture below if desired.

architecture="arm64"
install_root="$(pwd)/build/deb/switch-initramfs"
package_version="1.0-1"

git clone https://gitlab.com/switchroot/kernel/l4t-initramfs
cd l4t-initramfs
./build.sh
mkdir -p "${install_root}"/boot/ || exit
cp initramfs "${install_root}"/boot/

cd "$install_root" || exit

mkdir -p DEBIAN
cd DEBIAN || exit

cat <<EOF > control
Package: switch-initramfs
Architecture: $architecture
Maintainer: Azkali Manad
Priority: important
Version: $package_version
Description: Initramfs for Nintendo Switch
EOF

cat <<EOF > conffiles
EOF

cd ../..

dpkg-deb --build switch-initramfs || exit
mv switch-initramfs.deb switch-initramfs_${package_version}.deb
