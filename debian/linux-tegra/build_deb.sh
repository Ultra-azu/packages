#!/bin/bash

# This is a hacky little script to generate a deb.
#
# NOTE: This only works for building an arm64 .deb as is. Change
#       the Architecture below if desired.

architecture="arm64"
image="$(pwd)/build/deb/linux-switch"
devicetree="$(pwd)/build/deb/switch-device-tree"
modules="$(pwd)/build/deb/linux-switch-modules"
headers="$(pwd)/build/deb/linux-switch-headers"
package_version="4.9.140-1"

echo "Building kernel"
docker run --rm -it -e ARCH=arm64 -e CPUS=$(nproc) -v $(pwd):/out registry.gitlab.com/switchroot/kernel/l4t-kernel-build-scripts:latest || exit

mkdir -p "${image}"/boot \
	"${devicetree}"/boot \
	"${modules}"/usr/lib/firmware/brcm \
	"${headers}"
tar xf modules.tar.gz -C "${modules}"/usr/lib/
tar xf update.tar.gz -C "${headers}"
cp brcmfmac4356-pcie.txt "${modules}"/usr/lib/firmware/brcm/
install -Dm 755 Image "${image}"/boot
install -Dm 755 tegra210-icosa.dtb "${devicetree}"/boot

cd "$image" || exit
mkdir -p DEBIAN
cd DEBIAN || exit

cat <<EOF > control
Package: linux-switch
Architecture: $architecture
Maintainer: Azkali Manad
Priority: important
Version: $package_version
Description: Linux kernel Image for Nintendo Switch
EOF

cat <<EOF > conffiles
EOF

cd "$devicetree" || exit
mkdir -p DEBIAN
cd DEBIAN || exit

cat <<EOF > control
Package: switch-device-tree
Architecture: $architecture
Maintainer: Azkali Manad
Priority: important
Version: $package_version
Description: Nintendo Switch device tree
EOF

cat <<EOF > conffiles
EOF

cd "$modules" || exit
mkdir -p DEBIAN
cd DEBIAN || exit

cat <<EOF > control
Package: linux-switch-modules
Architecture: $architecture
Maintainer: Azkali Manad
Priority: important
Version: $package_version
Description: Linux kernel modules for Nintendo Switch
EOF

cat <<EOF > conffiles
EOF

cd "$headers" || exit
mkdir -p DEBIAN
cd DEBIAN || exit

cat <<EOF > control
Package: linux-switch-headers
Architecture: $architecture
Maintainer: Azkali Manad
Priority: important
Version: $package_version
Description: Linux kernel headers for Nintendo Switch
EOF

cat <<EOF > conffiles
EOF

cd ../..

dpkg-deb --build switch-device-tree || exit
dpkg-deb --build linux-switch || exit
dpkg-deb --build linux-switch-modules || exit
dpkg-deb --build linux-switch-headers || exit

mv switch-device-tree.deb switch-device-tree_${package_version}.deb
mv linux-switch.deb linux-switch_${package_version}.deb
mv linux-switch-modules.deb linux-switch-modules_${package_version}.deb
mv linux-switch-headers.deb linux-switch-headers_${package_version}.deb

