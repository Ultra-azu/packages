#!/bin/bash

# This is a hacky little script to generate a deb.
#
# NOTE: This only works for building an arm64 .deb as is. Change
#       the Architecture below if desired.

architecture="arm64"
package="$(pwd)/build/deb/switch-alsa-ucm2"
package_version="1.0"

mkdir -p "${package}/usr/share/alsa/ucm2/tegra-snd-t210ref-mobile-rt565x/" "${package}/DEBIAN"

cp HiFi.conf \
	tegra-snd-t210ref-mobile-rt565x.conf \
	"${package}/usr/share/alsa/ucm2/tegra-snd-t210ref-mobile-rt565x/"
install -Dm 0775 postinst "${package}/DEBIAN/"

cd "${package}/DEBIAN" || exit

cat <<EOF > control
Package: switch-alsa-ucm2
Architecture: $architecture
Maintainer: Azkali Manad
Homepage: https://gitlab.com/switchroot
Depends: libasound2-data
Replaces: switch-alsa-ucm
Provides: switch-alsa-ucm
Priority: optional
Version: $package_version
Description: ALSA UCM2 configuration for the Nintendo Switch
EOF

cat <<EOF > conffiles
EOF

cd ../..

dpkg-deb --build switch-alsa-ucm2 || exit
mv switch-alsa-ucm2.deb switch-alsa-ucm2_${package_version}.deb
