#!/bin/bash

# This is a hacky little script to generate a deb.
#
# NOTE: This only works for building an arm64 .deb as is. Change
#       the Architecture below if desired.

architecture="arm64"
package="$(pwd)/build/deb/nvidia-l4t-postinst-hook"
package_version="1.0"

mkdir -p "${package}/DEBIAN"

install -Dm 0775 postinst "${package}/DEBIAN"

cd "${package}/DEBIAN" || exit

cat <<EOF > control
Package: nvidia-l4t-postinst-hook
Architecture: $architecture
Maintainer: Azkali Manad
Homepage: https://gitlab.com/switchroot
Priority: optional
Version: $package_version
Description: Nvidia L4T post installation hook for Nintendo Switch
EOF

cat <<EOF > conffiles
EOF

cd ../..

dpkg-deb --build nvidia-l4t-postinst-hook || exit
mv nvidia-l4t-postinst-hook.deb nvidia-l4t-postinst-hook_${package_version}.deb
