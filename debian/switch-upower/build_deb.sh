#!/bin/bash

# This is a hacky little script to generate a deb.
#
# NOTE: This only works for building an arm64 .deb as is. Change
#       the Architecture below if desired.

architecture="arm64"
package="$(pwd)/build/deb/switch-upower"
package_version="1.0"

mkdir -p "${package}/etc/systemd/system/"

cp upower.service "${package}/etc/systemd/system/"

cd "${package}" || exit

mkdir -p DEBIAN
cd DEBIAN || exit

cat <<EOF > control
Package: switch-upower
Architecture: $architecture
Maintainer: Azkali Manad
Homepage: https://gitlab.com/switchroot
Depends: upower
Priority: optional
Version: $package_version
Description: UPower profile for the Nintendo Switch
EOF

cat <<EOF > conffiles
EOF

cd ../..

dpkg-deb --build switch-upower || exit
mv switch-upower.deb switch-upower_${package_version}.deb
