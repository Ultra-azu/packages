#!/bin/bash

# This is a hacky little script to generate a deb.
#
# NOTE: This only works for building an arm64 .deb as is. Change
#       the Architecture below if desired.

architecture="arm64"
install_root="$(pwd)/build/deb/switch-boot-files-bin"
package_version="32.4.4-1"

if [[ ! $1 ]]; then
	echo "No distro name."
	exit 1
fi

DISTRO=$1
echo "Building bootfiles for $DISTRO"

docker run -it --rm -e CPUS=$(nproc) -e DISTRO=$DISTRO -v $(pwd):/out registry.gitlab.com/switchroot/bootstack/bootstack-build-scripts

mkdir -p "${install_root}" || exit

sudo mv ./bootloader ./switchroot "${install_root}" || exit

cd "$install_root" || exit

mkdir -p DEBIAN
cd DEBIAN || exit

cat <<EOF > control
Package: switch-boot-files-bin
Architecture: $architecture
Maintainer: Azkali Manad
Priority: important
Version: $package_version
Description: Nintendo Switch bootfiles
EOF

cat <<EOF > conffiles
EOF

cd ../..

fakeroot dpkg-deb --build switch-boot-files-bin || exit
mv switch-boot-files-bin switch-boot.deb-files-bin_${package_version}.deb
